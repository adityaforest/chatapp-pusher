import '@/styles/globals.css'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'

function App({ Component, pageProps }) {
  const [username, setUsername] = useState('')
  const router = useRouter()

  const handleLogin = (e) => {
    e.preventDefault()
    router.push('/chat')
  }

  useEffect(() => {
    if(username === ''){
      router.push('/')
    }
  }, [])

  return <Component handleLoginChange={(e) => setUsername(e.target.value)} username={username} handleLogin={handleLogin} {...pageProps} />
}

export default App
