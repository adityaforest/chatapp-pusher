import axios from 'axios'
import Pusher from 'pusher-js'
import { useEffect, useState } from 'react'

export default function Chat({ username }) {
    const pusher = new Pusher(process.env.NEXT_PUBLIC_key, {
        cluster: 'us3',
        authEndpoint: '/api/pusher/auth',
        auth: { params: { username } }
    })

    const [chats, setChats] = useState([])
    const [message, setMessage] = useState('')
    const [onlineUsers, setOnlineUsers] = useState([])
    const [onlineUsersCount, setOnlineUsersCount] = useState(0)

    useEffect(() => {
        let mounted = true
        if (mounted) {
            console.log('subscribing to pusher presence channel ...')
            const channel = pusher.subscribe('presence-channel')

            //when a user subscribe to a channel
            channel.bind('pusher:subscription_succeeded', (members) => {
                setOnlineUsersCount(members.count)
                console.log('subcription succeded')
                //console.log(members.members)
                let allMembers = []
                for (const userid in members.members) {
                    allMembers = [...allMembers, { id: userid, info: members.members[userid] }]
                }
                //console.log(allMembers)
                setOnlineUsers(allMembers)
            })

            //when a new member join the chat
            channel.bind('pusher:member_added', (member) => {
                setOnlineUsersCount(channel.members.count)
                console.log('member added')
                //console.log(member)
                setOnlineUsers((prevState) => [
                    ...prevState,
                    member
                ])
            })

            //when a member left the channel
            channel.bind('pusher:member_removed', (member) => {
                setOnlineUsersCount(channel.members.count)
                console.log('member removed')
                let allMembers = []
                for (const userid in channel.members.members) {
                    allMembers = [...allMembers, { id: userid, info: channel.members.members[userid] }]
                }
                //console.log(allMembers)
                setOnlineUsers(allMembers)
            })

            //when a member send a chat
            channel.bind('chat-update', (data) => {
                const { message, username } = data
                setChats((prevState) => [
                    ...prevState,
                    { username, message }
                ])
            })
        }

        return () => {
            pusher.unsubscribe('presence-channel')
            mounted = false
        }
    }, [])

    const handleSubmit = async (e) => {
        e.preventDefault()
        //await axios.post(`${process.env.NEXT_PUBLIC_thisurl}/api/pusher`, {
        await axios.post(`/api/pusher`, {
            message,
            username
        })
    }

    return (
        <div className='bg-blue-400 min-h-screen flex'>
            <div className='bg-blue-200 min-h-screen rounded-2xl p-2' style={{ width: '30%' }}>
                <div className='flex items-center border-b-2 border-white grow' style={{ height: '10%' }}>
                    <img className='' style={{ height: '50px', width: '50px' }} src="/logo.png" alt="" />
                    <div>
                        <h1 className='font-extrabold text-sm text-white'>Forest Chat App</h1>
                        <h3 className='text-purple-500'>There are {onlineUsersCount} users online !</h3>
                    </div>
                </div>
                <div className='flex items-center content-center border-b-2 border-white grow' style={{ height: '10%' }}>
                    <div className='bg-blue-400 rounded-full flex items-center justify-center' style={{ width: '45px', height: '45px' }}>
                        <h1 className='text-white font-extrabold'>FR</h1>
                    </div>
                    <div className='ml-5'>
                        <h1 className='font-bold'>{username}</h1>
                        <h1>useridhere</h1>
                    </div>
                </div>
                <div className='flex items-center justify-evenly bg-slate-500 rounded-2xl mt-2' style={{ height: '7%' }}>
                    <h1 className='font-extrabold ml-2 text-2xl'>My Chats</h1>
                    <button className='bg-blue-400 rounded-full flex items-center justify-center' style={{ width: '120px', height: '25px' }}>
                        <h1 className='font-bold text-lg'>+ new chat</h1>
                    </button>
                </div>
                <div className='bg-slate-400 rounded-2xl' style={{ height: '72%' }}>

                </div>
            </div>
            <div className='bg-blue-200 min-h-screen rounded-2xl p-2 mx-2' style={{ width: '70%' }}>
                <div className='bg-slate-500 rounded-2xl flex items-center justify-center' style={{ width: '100%', height: '10%' }}>
                    <h1 className='text-lg font-bold'>Public Room</h1>
                </div>
                <div className='bg-slate-400 rounded-2xl' style={{ width: '100%', height: '90%' }}>
                    <div className='p-2 flex flex-col justify-end' style={{ height: '90%' }}>
                        {chats.map((chat, id) => {
                            console.log(chat)
                            if (chat.username !== username) {
                                return (
                                    <div className='flex mb-4' key={id}>
                                        <div className='bg-blue-400 rounded-full flex items-center justify-center' style={{ width: '45px', height: '45px' }}>
                                            <h1 className='text-white font-extrabold'>FR</h1>
                                        </div>
                                        <div style={{ minWidth: '5%', width: 'fit-content', maxWidth: '70%', minWidth: '10%' }}>
                                            <div className='bg-slate-500 rounded-md p-1 ml-2'>
                                                <h5 className='text-sm font-bold'>{chat.username}</h5>
                                                <h1 className='' style={{ width: '100%', overflowWrap: 'break-word' }}>
                                                    {chat.message}
                                                </h1>
                                            </div>
                                            <div className='flex justify-end pr-2' style={{ width: '100%' }}>
                                                <h1 className='text-xs'>5 min ago</h1>
                                            </div>
                                        </div>
                                    </div>
                                )
                            }
                            else {
                                return (
                                    <div className='flex flex-col items-end mb-4' style={{ width: '100%' }} key={id}>
                                        <div className='bg-blue-500 rounded-md p-1 px-3' style={{ minWidth: '5%', width: 'fit-content', maxWidth: '70%' }}>
                                            <h1 style={{ width: '100%', overflowWrap: 'break-word' }}>
                                                {chat.message}
                                            </h1>
                                        </div>
                                        <div className='pr-2'>
                                            <h1 className='text-xs'>5 min ago</h1>
                                        </div>
                                    </div>
                                )
                            }
                        })}


                    </div>
                    <div className='flex items-center p-2' style={{ height: '10%' }}>
                        <form className='text-black' style={{ width: '100%' }} onSubmit={handleSubmit}>
                            <input className='rounded-md' style={{ width: '90%', padding: '5px' }} type="text" placeholder='enter a message'
                                onChange={(e) => setMessage(e.target.value)} />
                            <button className='rounded-md bg-blue-500 text-white ml-4' style={{ height: '30px', width: '60px' }}>Send</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}