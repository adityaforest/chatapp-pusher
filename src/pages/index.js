import Image from 'next/image'
import { Inter } from 'next/font/google'

const inter = Inter({ subsets: ['latin'] })

export default function Home({ handleLogin, handleLoginChange }) {
  return (
    <main className="flex min-h-screen flex-col items-center justify-content-center p-24">
      <div className='text-white mb-10'>
        <h1 className='mb-5'>Forest Chat App</h1>
        <p>1. No data will be recorded</p>
        <p>2. Your chat history will be deleted if you leave or
          refresh this page</p>
        <p>3. Please don't give any private information to strangers in this site</p>
      </div>
      <form onSubmit={handleLogin} >
        <input className='border-2 py-2 px-2 rounded-md text-black' type="text" placeholder='enter your username..' onChange={handleLoginChange} />
        <button className='bg-blue-500 py-2 px-2 rounded-md mx-2'>Submit</button>
      </form>
    </main>
  )
}
