import {pusher} from '../../../lib'

export default async function handler (req,res) {
    const {message , username} = req.body

    try{
        await pusher.trigger('presence-channel','chat-update',{
            message,
            username
        })
        console.log('pusher success')
        res.json({status:200})

    }catch(err){
        console.log('error sending message')
        console.log(err)
        res.json({status:500,err})
    }
}